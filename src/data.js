import { v4 as uuidv4 } from "uuid";
let english = "https://lh3.google.com/u/0/d/1v1yMN3Dan8lUuetS5vaKC7qEWCkcLdDh=w957-h818-iv1";
function chillHop() {
  return [
    {
      name: "American Boy",
      cover: english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/American-Boi.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Bad romance - Lady Gaga",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/BadRomance.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Before you go - Lewis Capaldi",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/BeforeUGo.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Best of me - BTS",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/BestOfMe_BTS.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Better - Zayn",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/BetterZayn.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Boulevard of broke dreams - Green Day",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/BolevBroken.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Can we kiss forever - Kina",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/CanWeKissForever.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Chandelier - Sia",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/Chanlier.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Circles - Post Malone",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/Circles.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Courage to change - Sia",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/CourageToChangeSia.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Heatbreak anniversary - Giveon",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/HeartbreakAnniversary.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Heathens - 21 Pilots",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/Heathens.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Interstellar theme song - Hans Zimmer",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/Interstellar.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Lonely - Justin Beiber & Benny Blanco",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/LoonelyJB.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Lovely - Billie Eilish",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/LovelyBillie.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Perfect - Ed Sheeran",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/PerfectEdSheeran.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Piano man - Billy Joel",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/PianoMan.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Pirates of the Carribean - Dead man tell no tales",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/DeadManTell.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Purpose - Justin Beiber",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/PurposeJB.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Runaway - Aurora",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/runaway.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "See you again - Wiz Khalifa ft. Charlie Puth",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/SeeYouAgain.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Someone you loved - Lewis Capaldi",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/SomeoneYouLoved.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Zombie - The Cranberries",
      cover:
        english,
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/English/TheCranberries.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },


    // Hindi
    {
      name: "Abaad barbaad - Arijit Singh",
      cover:
        "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/AbaadBarbaad.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Agar tum saath ho - Alka Yagnik & Arijit Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/AgarTumSaathHo.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Awari - Adnan Dhool, Momina Musteshan",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/Awari.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Baatei ye kabhi na - Arijit Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/BaateiYeKabhiNa.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Bheegi bheegi - Neha Kakkar, Tony Kakkar",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/BheegiBheegiNK.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Bolna - Arijit, Asees & Tankishk",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/Bolna.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Chal wahin chalein - Shreya Ghoshal, Amaal Malik",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/ChalWahinChalei.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Dil diyan gallan - Atif Aslam, Vishal and Shekhar",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/DilDiyaGallan.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Dil tod ke - B Praak",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/DilTodKe.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Ek ladki bheegi bhaagi si - Kishore Kumar",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/EkLadkiBheegi.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Filhaal - BPraak",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/Filhaal.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Hamari adhuri kahaani - Arijit Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/HmariAdhuriKahani.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Haule haule - Sukhwinder Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/HauleHaule.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Ik Vaari - Pritam, Arijit Singh & Amitabh B",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/IkVaari.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Ik mulakaat - Meet Bros Ft. Altamash F & Palak M",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/IkMulakaat.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Is qadar - Tulsi Kumar & Darshan Raval",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/IsQadar.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Itti si hasi - Pritam",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/ittisihasi.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Jeena jeena - Atif Aslam",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/jeenajeena.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Kal ho na ho - Sonu Nigam",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/kalhonaho.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Kalank title track - Arijit Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/kalank.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Kaun tujhe - Palak Muchhal",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/kauntujhe.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Khairiyat - Nitesh Tiwari, Arijit",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/khairiyat.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Khamoshiyan - Arijit",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/khamoshiyan.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Mere humsafar - Mithoon, Tulsi Kumar",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/merehumsafar.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Naina - Arijit",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/naina.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "O saathi - Atif Aslam",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/osaathi.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Pal - Arijit Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/pal.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Pehla Nasha - Udit Narayan",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/pehlanasha.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Pehla pehla pyar - Armaan Malik",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/pehlapehlapyar.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Phir bhi tumko chahunga - Arijit Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/firbhitumko.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Phir Chala - Payal Dev, Jubin Nautiyal",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/phirchala.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Phir kabhi - Arijit Singh",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/phirkabhi.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Scam 1992 theme song - Achint",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/scam1992.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Teri mitti - B Praak",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/terimitti.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Tujhe kitna - Arijit",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/tujhekitna.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Tum hi ho - Arijit",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/tumhiho.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Tum se hi - Ankit Tiwari, Leena Bose",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/tumsehi.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    {
      name: "Waqt ki baatein - Dream Note",
      cover:
      "https://lh3.google.com/u/0/d/1ZyWt5qg2JSK6MeNRR30tWQaesC7Wk9X3=w1919-h954-iv1",
      artist: "Himanshu",
      audio: "https://bot-7037.github.io/PianoLibrary/SongsPage/Hindi/waqtkibaatein.mp3",
      color: ["#205950", "#2ab3bf"],
      id: uuidv4(),
      active: true,
    },
    
    //ADD MORE HERE
  ];
}

export default chillHop;
